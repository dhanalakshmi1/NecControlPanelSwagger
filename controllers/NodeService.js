'use strict';

exports.deleteNode = function(args, res, next) {
  /**
   * Removes a Node (FOR INTERNAL USE ONLY)
   * 
   *
   * hostname String Hostname of the node
   * no response value expected for this operation
   **/
  res.end();
}

exports.getClient = function(args, res, next) {
  /**
   * It gets an updated version of execution framework to run on the Node (FOR INTERNAL USE ONLY)
   * This is only called by the worker node.
   *
   * returns String
   **/
  var examples = {};
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

exports.getFluentAggregatorConfiguration = function(args, res, next) {
  /**
   * Get the configuration for fluentd aggregator daemon running on the master node (FOR INTERNAL USE ONLY)
   * 
   *
   * returns String
   **/
  var examples = {};
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

exports.getFluentConfiguration = function(args, res, next) {
  /**
   * Get the configuration for fluentd daemon running on worker nodes (FOR INTERNAL USE ONLY)
   * 
   *
   * returns String
   **/
  var examples = {};
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

exports.getNodes = function(args, res, next) {
  /**
   * Retrieves the list of registered Node(s)
   * 
   *
   * locationId Long Filter out results by locationId. -1 means showing all nodes while 0 means showing nodes that are not associated to any location. (optional)
   * returns List
   **/
  var examples = {};
  examples['application/json'] = [ {
  "diskAvailable" : 123456789,
  "memoryUsage" : 1.3579000000000001069366817318950779736042022705078125,
  "ip" : "aeiou",
  "type" : "aeiou",
  "latestUpdate" : "2000-01-23T04:56:07.000+00:00",
  "hostname" : "aeiou",
  "numberOfInstances" : 123,
  "diskSize" : 123456789,
  "cpuLoad" : 1.3579000000000001069366817318950779736042022705078125,
  "memorySize" : 123456789,
  "cpuCores" : 123,
  "locationId" : 123456789,
  "memoryAvailable" : 123456789,
  "diskUsage" : 1.3579000000000001069366817318950779736042022705078125,
  "ipManuallySet" : true,
  "status" : "aeiou"
} ];
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

exports.getRegistryCredential = function(args, res, next) {
  /**
   * Get the credential of the registry server from the master node (FOR INTERNAL USE ONLY)
   * 
   *
   * returns Object
   **/
  var examples = {};
  examples['application/json'] = "{}";
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

exports.getSslCertificate = function(args, res, next) {
  /**
   * Get the SSL certificate of the registry server from the master node (FOR INTERNAL USE ONLY)
   * 
   *
   * returns String
   **/
  var examples = {};
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

exports.getWindowsServiceTemplate = function(args, res, next) {
  /**
   * Get the windows service template from the master node. This is used by worker controller on Windows (FOR INTERNAL USE ONLY).
   * 
   *
   * returns String
   **/
  var examples = {};
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

exports.setNodeInfo = function(args, res, next) {
  /**
   * Set information of the node
   * 
   *
   * hostname String Hostname of the node
   * body SetNodeInfoMessage The information to associate to the node
   * no response value expected for this operation
   **/
  res.end();
}

exports.setNodeLocation = function(args, res, next) {
  /**
   * Set location of the node
   * 
   *
   * hostname String Hostname of the node
   * body SetNodeLocationMessage Location of the node. 0 means that the node is not associated to any location.
   * no response value expected for this operation
   **/
  res.end();
}

exports.updateNode = function(args, res, next) {
  /**
   * Updates the information about a Node (FOR INTERNAL USE ONLY)
   * 
   *
   * body NodeReport The report from the node (optional)
   * returns RequestToNode
   **/
  var examples = {};
  examples['application/json'] = {
  "stop" : [ 123456789 ],
  "start" : [ 123456789 ]
};
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

