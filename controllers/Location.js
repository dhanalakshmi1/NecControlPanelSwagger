'use strict';

var url = require('url');

var Location = require('./LocationService');

module.exports.addLocation = function addLocation (req, res, next) {
  Location.addLocation(req.swagger.params, res, next);
};

module.exports.addLocationMembers = function addLocationMembers (req, res, next) {
  Location.addLocationMembers(req.swagger.params, res, next);
};

module.exports.getLocation = function getLocation (req, res, next) {
  Location.getLocation(req.swagger.params, res, next);
};

module.exports.getLocations = function getLocations (req, res, next) {
  Location.getLocations(req.swagger.params, res, next);
};

module.exports.removeLocation = function removeLocation (req, res, next) {
  Location.removeLocation(req.swagger.params, res, next);
};

module.exports.removeLocationMember = function removeLocationMember (req, res, next) {
  Location.removeLocationMember(req.swagger.params, res, next);
};

module.exports.updateLocation = function updateLocation (req, res, next) {
  Location.updateLocation(req.swagger.params, res, next);
};
