'use strict';
var low	= require('lowdb');
const FileSync = require('lowdb/adapters/FileSync')

const adapter = new FileSync('db.json')
const db = low(adapter);
// Load the full build. 
var _ = require('lodash');
// Load the core build. 
var _ = require('lodash/core');
exports.addAccount = function(args, res, next) {
  /**
   * Create a new user
   * 
   *
   * body User The new user to insert
   * returns User
   **/
  var examples = {};
  examples['application/json'] = {
  "password" : "aeiou",
  "role" : "aeiou",
  "permissions" : [ {
    "resource" : "aeiou",
    "privilege" : "aeiou"
  } ],
  "fullName" : "aeiou",
  "username" : "aeiou"
};
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

exports.getAccounts = function(args, res, next) {
  /**
   * Get a list of users
   * 
   *
   * returns List
   **/
 res.setHeader('Content-Type', 'application/json');
  res.statusCode = 200;
  var  result = db.get('user').value();
	 res.end(JSON.stringify(result));
}

exports.removeAccount = function(args, res, next) {
  /**
   * Delete an user
   * 
   *
   * username String Username of the user account. It can only contain 6 ~ 20 letters and numbers
   * no response value expected for this operation
   **/
  res.end();
}

exports.updateAccount = function(args, res, next) {
  /**
   * Modify an user
   * 
   *
   * username String Username of the user account. It can only contain 6 ~ 20 letters and numbers
   * body UserProfile Updated information about the user
   * returns User
   **/
  var examples = {};
  examples['application/json'] = {
  "password" : "aeiou",
  "role" : "aeiou",
  "permissions" : [ {
    "resource" : "aeiou",
    "privilege" : "aeiou"
  } ],
  "fullName" : "aeiou",
  "username" : "aeiou"
};
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

