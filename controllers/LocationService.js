'use strict';
var low	= require('lowdb');
const FileSync = require('lowdb/adapters/FileSync')

const adapter = new FileSync('db.json')
const db = low(adapter);
// Load the full build. 
var _ = require('lodash');
// Load the core build. 
var _ = require('lodash/core');
exports.addLocation = function(args, res, next) {
  /**
   * Add a new location
   * 
   *
   * body LocationParameters The new location to insert
   * returns LocationBasicInfo
   **/
  var examples = {};
  examples['application/json'] = {
  "name" : "aeiou",
  "id" : 123456789,
  "type" : "aeiou",
  "info" : {
    "key" : "{}"
  }
};
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

exports.addLocationMembers = function(args, res, next) {
  /**
   * Add a list of locations as members of this location
   * 
   *
   * locationId Long Unique ID of the location
   * body List A list of location IDs
   * no response value expected for this operation
   **/
  res.end();
}

exports.getLocation = function(args, res, next) {
  /**
   * Retrieve a location's information
   * 
   *
   * locationId Long Unique ID of the location
   * returns Location
   **/
  res.setHeader('Content-Type', 'application/json');
  res.statusCode = 200;
  var  result = db.get('location').value();

   if(args.locationId.value !== undefined) {
	
	    result = _.filter(result, {id: args.locationId.value});
	}
	res.end(JSON.stringify(result));
}

exports.getLocations = function(args, res, next) {
  /**
   * Retrieve all locations
   * 
   *
   * type String Type of the location, e.g., building, room, gate, etc. If specified, list only locations of the specific type. (optional)
   * returns List
   **/
  res.setHeader('Content-Type', 'application/json');
  res.statusCode = 200;
  var  result = db.get('location').value();
	
   if(args.type.value !== undefined) {
	    result = _.filter(result, {type: args.type.value});
	}
	res.end(JSON.stringify(result));
}

exports.removeLocation = function(args, res, next) {
  /**
   * Remove a location
   * 
   *
   * locationId Long Unique ID of the location
   * no response value expected for this operation
   **/
  res.end();
}

exports.removeLocationMember = function(args, res, next) {
  /**
   * Remove a member from a location
   * 
   *
   * locationId Long Unique ID of the location
   * memberId Long locationId of the member
   * no response value expected for this operation
   **/
  res.end();
}

exports.updateLocation = function(args, res, next) {
  /**
   * Update the information of a location
   * 
   *
   * locationId Long Unique ID of the location
   * body LocationParameters The new parameters to assign to the location
   * no response value expected for this operation
   **/
  res.end();
}

