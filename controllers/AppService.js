'use strict';
var low	= require('lowdb');
const FileSync = require('lowdb/adapters/FileSync')

const adapter = new FileSync('db.json')
const db = low(adapter);
// Load the full build. 
var _ = require('lodash');
// Load the core build. 
var _ = require('lodash/core');
exports.getApp = function(args, res, next) {
  /**
   * Retrieves the installed App by appId
   * 
   *
   * appId String Unique ID of the app, e.g., example-helloworld
   * returns AppDescription
   **/
 res.setHeader('Content-Type', 'application/json');
  res.statusCode = 200;
  var  result = db.get('apps').value();
	  result = _.filter(result,{id: args.appId.value});
	 res.end(JSON.stringify(result));
}

exports.getAppBundle = function(args, res, next) {
  /**
   * Download the App bundle.
   * 
   *
   * appId String Unique ID of the app, e.g., example-helloworld
   * no response value expected for this operation
   **/
  res.end();
}

exports.getAppReadme = function(args, res, next) {
  /**
   * Retrieve README.md file of an app
   * 
   *
   * appId String Unique ID of the app, e.g., example-helloworld
   * no response value expected for this operation
   **/
  res.end();
}

exports.getAppType = function(args, res, next) {
  /**
   * Get the app type (docker or native).
   * 
   *
   * appId String Unique ID of the app, e.g., example-helloworld
   * no response value expected for this operation
   **/
  res.end();
}

exports.getApps = function(args, res, next) {
 res.setHeader('Content-Type', 'application/json');
  res.statusCode = 200;
  var  result = db.get('apps').value();
	 res.end(JSON.stringify(result));
}

exports.getConfigurationTemplate = function(args, res, next) {
  /**
   * Retrieve a configuration template for the App
   * 
   *
   * appId String Unique ID of the app, e.g., example-helloworld
   * no response value expected for this operation
   **/
  res.end();
}

exports.installApp = function(args, res, next) {
  /**
   * Install App
   * 
   *
   * file File zip file containing the App
   * returns AppId
   **/
  var examples = {};
  examples['application/json'] = {
  "appId" : "aeiou"
};
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

exports.removeApp = function(args, res, next) {
  /**
   * Uninstall App
   * 
   *
   * appId String Unique ID of the app, e.g., example-helloworld
   * no response value expected for this operation
   **/
  res.end();
}

exports.updateApp = function(args, res, next) {
  /**
   * Update App
   * 
   *
   * file File zip file containing the App
   * returns AppId
   **/
  var examples = {};
  examples['application/json'] = {
  "appId" : "aeiou"
};
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

