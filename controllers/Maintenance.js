'use strict';

var url = require('url');

var Maintenance = require('./MaintenanceService');

module.exports.backupSystem = function backupSystem (req, res, next) {
  Maintenance.backupSystem(req.swagger.params, res, next);
};

module.exports.restoreSystem = function restoreSystem (req, res, next) {
  Maintenance.restoreSystem(req.swagger.params, res, next);
};
