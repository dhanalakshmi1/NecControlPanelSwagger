'use strict';
var low	= require('lowdb');
const FileSync = require('lowdb/adapters/FileSync')

const adapter = new FileSync('db.json')
const db = low(adapter);
// Load the full build. 
var _ = require('lodash');
// Load the core build. 
var _ = require('lodash/core');

exports.deleteFile = function(args, res, next) {
  /**
   * Remove a file
   * 
   *
   * instanceId Long Unique ID of the instance
   * fileName String File name
   * no response value expected for this operation
   **/
  res.end();
}

exports.deleteInstance = function(args, res, next) {
  /**
   * Remove an Instance
   * 
   *
   * instanceId Long Unique ID of the instance
   * force Boolean whether to force remove a running instance (optional)
   * no response value expected for this operation
   **/
  res.end();
}

exports.getConnectionCandidates = function(args, res, next) {
  /**
   * Get a list of endpoints this endpoint can connect to
   * 
   *
   * instanceId Long Unique ID of the instance
   * name String Name of the endpoint
   * returns List
   **/
	  res.setHeader('Content-Type', 'application/json');
  res.statusCode = 200;
  res.end(JSON.stringify(db.get('instances').value()));

	
  /*var examples = {};
  examples['application/json'] = [ {
  "instanceId" : 123456789,
  "endpointName" : "aeiou"
} ];
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }*/
}

exports.getEndpointSocketInfo = function(args, res, next) {
  /**
   * Get the socket type (binding or connecting) of this endpoint. If this endpoint's socket type is connecting, include the endpoint it connects to.
   * 
   *
   * instanceId Long Unique ID of the instance
   * name String Name of the endpoint
   * returns EndpointSocketInfo
   **/
  var examples = {};
  examples['application/json'] = {
  "socketType" : "aeiou",
  "address" : "aeiou",
  "instanceId" : 123456789,
  "endpointName" : "aeiou"
};
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

exports.getInstance = function(args, res, next) {
  /**
   * Retrieve the Instance by Id
   * 
   *
   * instanceId Long Unique ID of the instance
   * returns GetInstanceMessage
   **/
	console.log("getInstance method");
  res.setHeader('Content-Type', 'application/json');
  res.statusCode = 200;
  var  result = db.get('instances').value();
	if(args.instanceId.value ===undefined)
		console.log("object undefined");
   if(args.instanceId.value !== undefined) {
	    result = _.filter(result,{id: args.instanceId.value});
	  
	}
	
	
	 res.end(JSON.stringify(result));

}

exports.getInstanceConfiguration = function(args, res, next) {
	console.log("getInstanceConfiguration method");
  res.setHeader('Content-Type', 'application/json');
  res.statusCode = 200;
  var  result = db.get('instances').value();
	result = _.filter(result,{id: args.instanceId.value});
	result = result[0].parameters.configuration;
	 res.end(JSON.stringify(result));
}

exports.getInstanceConnections = function(args, res, next) {
  /**
   * Get a list of instances this instance connects to or is connected to
   * 
   *
   * instanceId Long Unique ID of the instance
   * returns List
   **/
  var examples = {};
  examples['application/json'] = [ {
  "socketType" : "aeiou",
  "endpointName" : "aeiou",
  "type" : "aeiou",
  "connections" : [ {
    "instanceId" : 123456789,
    "endpointName" : "aeiou"
  } ]
} ];
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

exports.getInstanceExpandedConfiguration = function(args, res, next) {
  /**
   * Retrieve an expanded configuration of the Instance, with detailed sensor/location information and database connection string
   * 
   *
   * instanceId Long Unique ID of the instance
   * no response value expected for this operation
   **/
  res.end();
}

exports.getInstanceFile = function(args, res, next) {
  /**
   * Retrieve a file associated to the Instance specified by Id
   * 
   *
   * instanceId Long Unique ID of the instance
   * fileName String File name
   * no response value expected for this operation
   **/
  res.end();
}

exports.getInstanceFiles = function(args, res, next) {
  /**
   * Retrieve a list of files associated with the Instance
   * 
   *
   * instanceId Long Unique ID of the instance
   * returns List
   **/
  var examples = {};
  examples['application/json'] = [ "aeiou" ];
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

exports.getInstances = function(args, res, next) {
  res.setHeader('Content-Type', 'application/json');
  res.statusCode = 200;
  var  result = db.get('instances').value();
	if(args.appId.value ===undefined)
		console.log("object undefined");
   if(args.appId.value !== undefined) {
	   console.log("args.appId.value="+args.appId.value);
	    result = _.filter(result, {parameters: {appId: args.appId.value}});
	}
	
	 if(args.hostname.value !== undefined) {
		 console.log("args.hostname.value="+args.hostname.value);
	      result = _.filter(result, {parameters: {appId: args.hostname.value}});
	}
	 if(args.sensorId.value !== undefined && args.sensorId.value > 0) {
		 console.log("args.sensorId.value="+args.sensorId.value);
	    result = _.filter(result, {parameters: {appId: args.sensorId.value}});
	}
	 res.end(JSON.stringify(result));
	
	
  /**
   * Retrieve the list of Instance(s)
   * 
   *
   * appId String Filter out results by the app ID, e.g., example-helloworld (optional)
   * hostname String Filter out results by the node's hostname (optional)
   * sensorId Long Only show instances that use the given sensor. -1 means showing all instances. (optional)
   * returns List
   **/

}

exports.postInstance = function(args, res, next) {
  /**
   * Create a new Instance
   * 
   *
   * body InstanceParameters The new instance to create
   * returns InstanceId
   **/
  var examples = {};
  examples['application/json'] = {
  "instanceId" : 123456789
};
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

exports.postInstanceStatus = function(args, res, next) {
  /**
   * Set the status of the Instance
   * Start or stop the Instance
   *
   * instanceId Long Unique ID of the instance
   * body InstanceSetStatusMessage The new status to assign to the instance
   * no response value expected for this operation
   **/
  res.end();
}

exports.setEndpointSocketInfo = function(args, res, next) {
  /**
   * Bind this endpoint to a local port or connect this endpoint to another endpoint (binding) specified in the body
   * 
   *
   * instanceId Long Unique ID of the instance
   * name String Name of the endpoint
   * body EndpointSocketParameters Socket information of the endpoint
   * no response value expected for this operation
   **/
  res.end();
}

exports.setNodes = function(args, res, next) {
  /**
   * Update the node where to run the Instance
   * 
   *
   * instanceId Long Unique ID of the instance
   * body InstanceNode The new node where to execute the instance (optional)
   * no response value expected for this operation
   **/
  res.end();
}

exports.updateEndpointStatus = function(args, res, next) {
  /**
   * Updates the timestamp of last message sent or received by this endpoint
   * 
   *
   * instanceId Long Unique ID of the instance
   * name String Name of the endpoint
   * body EndpointStatus The current status of the connection
   * no response value expected for this operation
   **/
  res.end();
}

exports.uploadInstanceFile = function(args, res, next) {
  /**
   * Upload a file for the Instance
   * 
   *
   * instanceId Long Unique ID of the instance
   * file File File to upload
   * no response value expected for this operation
   **/
  res.end();
}

