'use strict';
var low	= require('lowdb');
const FileSync = require('lowdb/adapters/FileSync')

const adapter = new FileSync('db.json')
const db = low(adapter)
// Load the full build. 
var _ = require('lodash');
// Load the core build. 
var _ = require('lodash/core');
exports.deleteToken = function(args, res, next) {
  /**
   * Delete a token. Call this for each logout.
   * 
   *
   * token String Security token of the user
   * no response value expected for this operation
   **/
  res.end();
}

exports.getActions = function(args, res, next) {
  /**
   * Get all actions
   * 
   *
   * returns List
   **/
  var examples = {};
  examples['application/json'] = [ {
  "resource" : "aeiou",
  "privilege" : "aeiou"
} ];
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

exports.getPermissions = function(args, res, next) {
  /**
   * Get permissions associated with the token
   * 
   *
   * token String Security token of the user
   * returns List
   **/
  var examples = {};
  examples['application/json'] = [ {
  "resource" : "aeiou",
  "privilege" : "aeiou"
} ];
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}
function generateUUID () { // Public Domain/MIT
    var d = new Date().getTime();
    if (typeof performance !== 'undefined' && typeof performance.now === 'function'){
        d += performance.now(); //use high-precision timer if available
    }
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
}
exports.getToken = function(args, res, next) {
	console.log("Inside::getToken");
  /**
   * Get a token for this user
   * 
   *
   * username String Username of the user account. It can only contain 6 ~ 20 letters and numbers
   * password String md5 encoded password
   * returns String
   **/
	res.end(generateUUID ());
}

