'use strict';

exports.getVersion = function(args, res, next) {
  /**
   * Get the version of the execution framework.
   * 
   *
   * returns String
   **/
  var examples = {};
  if (Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  } else {
    res.end();
  }
}

