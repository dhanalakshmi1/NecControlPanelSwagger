'use strict';

var url = require('url');

var Instance = require('./InstanceService');

module.exports.deleteFile = function deleteFile (req, res, next) {
  Instance.deleteFile(req.swagger.params, res, next);
};

module.exports.deleteInstance = function deleteInstance (req, res, next) {
  Instance.deleteInstance(req.swagger.params, res, next);
};

module.exports.getConnectionCandidates = function getConnectionCandidates (req, res, next) {
  Instance.getConnectionCandidates(req.swagger.params, res, next);
};

module.exports.getEndpointSocketInfo = function getEndpointSocketInfo (req, res, next) {
  Instance.getEndpointSocketInfo(req.swagger.params, res, next);
};

module.exports.getInstance = function getInstance (req, res, next) {
	
  Instance.getInstance(req.swagger.params, res, next);
};

module.exports.getInstanceConfiguration = function getInstanceConfiguration (req, res, next) {
  Instance.getInstanceConfiguration(req.swagger.params, res, next);
};

module.exports.getInstanceConnections = function getInstanceConnections (req, res, next) {
  Instance.getInstanceConnections(req.swagger.params, res, next);
};

module.exports.getInstanceExpandedConfiguration = function getInstanceExpandedConfiguration (req, res, next) {
  Instance.getInstanceExpandedConfiguration(req.swagger.params, res, next);
};

module.exports.getInstanceFile = function getInstanceFile (req, res, next) {
  Instance.getInstanceFile(req.swagger.params, res, next);
};

module.exports.getInstanceFiles = function getInstanceFiles (req, res, next) {
  Instance.getInstanceFiles(req.swagger.params, res, next);
};

module.exports.getInstances = function getInstances (req, res, next) {
  Instance.getInstances(req.swagger.params, res, next);
};

module.exports.postInstance = function postInstance (req, res, next) {
  Instance.postInstance(req.swagger.params, res, next);
};

module.exports.postInstanceStatus = function postInstanceStatus (req, res, next) {
  Instance.postInstanceStatus(req.swagger.params, res, next);
};

module.exports.setEndpointSocketInfo = function setEndpointSocketInfo (req, res, next) {
  Instance.setEndpointSocketInfo(req.swagger.params, res, next);
};

module.exports.setNodes = function setNodes (req, res, next) {
  Instance.setNodes(req.swagger.params, res, next);
};

module.exports.updateEndpointStatus = function updateEndpointStatus (req, res, next) {
  Instance.updateEndpointStatus(req.swagger.params, res, next);
};

module.exports.uploadInstanceFile = function uploadInstanceFile (req, res, next) {
  Instance.uploadInstanceFile(req.swagger.params, res, next);
};
