'use strict';

var url = require('url');

var Authorization = require('./AuthorizationService');

module.exports.deleteToken = function deleteToken (req, res, next) {
  Authorization.deleteToken(req.swagger.params, res, next);
};

module.exports.getActions = function getActions (req, res, next) {
  Authorization.getActions(req.swagger.params, res, next);
};

module.exports.getPermissions = function getPermissions (req, res, next) {
  Authorization.getPermissions(req.swagger.params, res, next);
};

module.exports.getToken = function getToken (req, res, next) {
  Authorization.getToken(req.swagger.params, res, next);
};
