'use strict';

var url = require('url');

var Database = require('./DatabaseService');

module.exports.addDatabase = function addDatabase (req, res, next) {
  Database.addDatabase(req.swagger.params, res, next);
};

module.exports.addNodeToDatabase = function addNodeToDatabase (req, res, next) {
  Database.addNodeToDatabase(req.swagger.params, res, next);
};

module.exports.getDatabase = function getDatabase (req, res, next) {
  Database.getDatabase(req.swagger.params, res, next);
};

module.exports.getDatabaseBackupFile = function getDatabaseBackupFile (req, res, next) {
  Database.getDatabaseBackupFile(req.swagger.params, res, next);
};

module.exports.getDatabases = function getDatabases (req, res, next) {
  Database.getDatabases(req.swagger.params, res, next);
};

module.exports.removeDatabase = function removeDatabase (req, res, next) {
  Database.removeDatabase(req.swagger.params, res, next);
};

module.exports.removeNodeFromDatabase = function removeNodeFromDatabase (req, res, next) {
  Database.removeNodeFromDatabase(req.swagger.params, res, next);
};

module.exports.restoreDatabase = function restoreDatabase (req, res, next) {
  Database.restoreDatabase(req.swagger.params, res, next);
};
